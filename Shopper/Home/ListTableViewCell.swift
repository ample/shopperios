//
//  ListTableViewCell.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/19/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var checkButton: UIButton!
    
    var item:ListItem!
    var homeVC:HomeViewController!
    var checked = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func checkTap(_ sender: Any) {
        
        DispatchQueue.main.async {
            self.checked = true
            self.homeVC.tableView.reloadData()
        }
        
        self.homeVC.api.removeItem(item_id: item.id) { (response:Dictionary) in
            DispatchQueue.main.async {
                self.checked = false
                self.homeVC.getList()
            }
        }
    }
    
    @IBAction func moreTap(_ sender: Any) {
        self.homeVC.showSortingSelector(item: self.item)
    }
    
}
