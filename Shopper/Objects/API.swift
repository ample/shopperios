//
//  API.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/19/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
class API: NSObject {

    let base = "http://97.107.128.142/shopper/api/public/"
    
    let headers: HTTPHeaders = [
        "apitoken": "3c206498-ba02-4725-ae09-80e63395978b",
        "Content-Type":"application/x-www-form-urlencoded"
    ]
    
    override init() {
        super.init()
        
        
    }
    
    public func login(email:String,password:String,completion:@escaping (_ response:Dictionary<String,Any>)->Void){
        
        let parameters: Parameters = [
            "email":email,
            "password":password
        ]
        
        Alamofire.request(base + "auth/login", method: .post, parameters: parameters, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
               status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
            ])
        }
    }
    
    public func signup(name:String,email:String,password:String,completion:@escaping (_ response:Dictionary<String,Any>)->Void){
        
        let parameters: Parameters = [
            "name":name,
            "email":email,
            "password":password
        ]
        
        Alamofire.request(base + "auth/signup", method: .post, parameters: parameters, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
                status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
                ])
        }
    }
    
    public func getList(userId:Int,completion:@escaping (_ response:Dictionary<String,Any>)->Void){

        
        Alamofire.request(base + "list/?user_id=" + String(userId), method: .get, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
                status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
            ])
        }
    }
    
    public func productSearch(q:String,user_id:Int,completion:@escaping (_ response:Dictionary<String,Any>)->Void){
        
        
        Alamofire.request(base + "product/search/?q=" + q.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)! + "&user_id=" + String(user_id), method: .get, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
                status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
            ])
        }
    }
    
    public func addItem(userId:Int,categoryId:Int,productId:Int,completion:@escaping (_ response:Dictionary<String,Any>)->Void){
        
        let parameters: Parameters = [
            "user_id":userId,
            "category_id":categoryId,
            "product_id":productId
        ]
        
        Alamofire.request(base + "list/add", method: .post, parameters: parameters, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
                status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
                ])
        }
    }
    
    public func createProduct(productName:String,completion:@escaping (_ response:Dictionary<String,Any>)->Void){
        
        let parameters: Parameters = [
            "productName":productName
        ]
        
        Alamofire.request(base + "product/create", method: .post, parameters: parameters, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
                status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
                ])
        }
    }
    
    public func removeItem(item_id:Int,completion:@escaping (_ response:Dictionary<String,Any>)->Void){
        
        let parameters: Parameters = [
            "item_id":item_id
        ]
        
        Alamofire.request(base + "list/remove", method: .post, parameters: parameters, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
                status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
            ])
        }
    }
    
    public func getCategories(completion:@escaping (_ response:Dictionary<String,Any>)->Void){
        
        
        Alamofire.request(base + "category", method: .get, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
                status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
                ])
        }
    }
    
    public func addRule(product_id:Int, category_id:Int, user_id:Int,completion:@escaping (_ response:Dictionary<String,Any>)->Void){
        
        let parameters: Parameters = [
            "product_id":product_id,
            "category_id":category_id,
            "user_id":user_id
        ]
        
        Alamofire.request(base + "rule/add", method: .post, parameters: parameters, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
                status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
                ])
        }
    }
    
    public func clearList(user_id:Int,completion:@escaping (_ response:Dictionary<String,Any>)->Void){
        
        let parameters: Parameters = [
            "user_id":user_id
        ]
        
        Alamofire.request(base + "list/clear", method: .post, parameters: parameters, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
                status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
                ])
        }
    }
    
    public func storeDevice(user_id:Int,completion:@escaping (_ response:Dictionary<String,Any>)->Void){
        
        let utils = Utils()
        
        let parameters: Parameters = [
            "device_udid":utils.getDeviceID(),
            "user_id":user_id,
            "phone_type":utils.getPhoneType(),
            "app_version":utils.getAppVersion(),
            "os_version":utils.getOSVersion()
        ]
        
        Alamofire.request(base + "device/store", method: .post, parameters: parameters, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
                status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
                ])
        }
    }
    
    public func log(user_id:Int,type:String,completion:@escaping (_ response:Dictionary<String,Any>)->Void){
        
        let utils = Utils()
        
        let parameters: Parameters = [
            "device_udid":utils.getDeviceID(),
            "user_id":user_id,
            "type":type
        ]
        
        Alamofire.request(base + "log", method: .post, parameters: parameters, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
                status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
                ])
        }
    }
    
    public func changePassword(user_id:Int,oldPassword:String,newPassword:String,completion:@escaping (_ response:Dictionary<String,Any>)->Void){
        
        let parameters: Parameters = [
            "user_id":user_id,
            "old_password":oldPassword,
            "new_password":newPassword
        ]
        
        Alamofire.request(base + "auth/changePassword", method: .post, parameters: parameters, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
                status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
                ])
        }
    }
    
    public func getSetting(key:String,completion:@escaping (_ response:Dictionary<String,Any>)->Void){
        
        
        Alamofire.request(base + "settings?key=" + key.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!, method: .get, headers:headers).responseJSON { response in
            var status = false
            if(response.response?.statusCode == 200){
                status = true
            }
            
            completion([
                "status":status,
                "data":self.parseResponse(data: response.data!)
                ])
        }
    }
    
    
    public func parseResponse(data:Data)->JSON{
        let json = JSON(data)
        return json
    }
}
