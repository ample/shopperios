//
//  ClearListViewController.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/20/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMobileAds
class ClearListViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var clearContainer: UIView!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var areYouSureLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var adContainer: UIView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    let utils = Utils()
    let constants = Constants()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.clearContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(clear)))
        
        self.areYouSureLabel.text = "Are you sure you want to clear your list? \n\n This cannot be undone."
        
        configureBanner()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.containerView.suttleCurve()
        self.clearContainer.suttleCurve()
       // self.adContainer.suttleCurve()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        bannerView.load(getAdRequest())
    }
    
    @objc func clear(){
        self.clearTap(self)
    }
    
    @IBAction func clearTap(_ sender: Any) {
        API().clearList(user_id: Settings().get_user().id) { (response:Dictionary) in
            let status = (response["status"] as! Bool)
            if(status){
                DispatchQueue.main.async {
                    self.closeTap(self)
                }
            }else{
                let json = (response["data"] as! JSON).dictionaryValue
                let message = json["message"]?.stringValue ?? ""
                DispatchQueue.main.async {
                    self.utils.showError(title:"Error",message:message)
                }
            }
        }
    }
    
    @IBAction func closeTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getAdRequest()->GADRequest{
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        return request
    }
    
    func configureBanner(){
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        if(self.constants.liveAd){
            //bannerView.adUnitID = "ca-app-pub-4573097868659019/4523591802"
        }
        bannerView.rootViewController = self
        bannerView.delegate = self
    }
}

extension ClearListViewController:GADBannerViewDelegate{
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Got ad")
        self.utils.logAdView()
    }
}
