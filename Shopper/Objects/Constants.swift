//
//  Constants.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/17/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit

class Constants: NSObject {

    let purple = UIColor.init(red: 109/255.0, green: 123/255.0, blue: 236/255.0, alpha: 1.0)
    let liveAd = false
}
