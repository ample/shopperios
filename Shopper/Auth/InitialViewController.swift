//
//  InitialViewController.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/24/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {

    @IBOutlet weak var logoContainer: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var signupContainer: UIView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var loginContainer: UIView!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var privacyPolicyButton: UIButton!
    @IBOutlet weak var termsOfServiceButton: UIButton!
    
    let settings = Settings()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(login)))
        self.signupContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(signup)))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let user = self.settings.get_user()
        if(user.id >= 0){
            home()
            return
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.containerView.suttleCurve()
        self.logoContainer.suttleCurve()
        self.signupContainer.suttleCurve()
        self.loginContainer.suttleCurve()
        
    }
    
    func home(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Home")
        self.view.window?.rootViewController = vc
    }
    
    @objc func login(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Login")
        self.view.window?.rootViewController = vc
    }
    
    @objc func signup(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Sign Up")
        self.view.window?.rootViewController = vc
    }

    @IBAction func signupTap(_ sender: Any) {
        signup()
    }
    @IBAction func loginTap(_ sender: Any) {
        login()
    }
    
    @IBAction func privacyPolicy(_ sender: Any){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Info") as! InfoViewController
        vc.titleStr = "Privacy Policy"
        vc.key = "privacy_policy"
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func termsOfServices(_ sender: Any){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Info") as! InfoViewController
        vc.titleStr = "Terms Of Service"
        vc.key = "terms_of_service"
        self.present(vc, animated: true, completion: nil)
    }
}
