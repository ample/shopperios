//
//  ListCat.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/19/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit

struct ListCat {

    var name:String = ""
    var items:[ListItem] = []
}
