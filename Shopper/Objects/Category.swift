//
//  Category.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/19/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit
import SwiftyJSON
struct Category {
    var id:Int = -1
    var name:String = ""
    
    mutating func build(json:JSON){
        let dict = json.dictionaryValue
        self.id = dict["id"]?.int ?? -1
        self.name = dict["name"]?.string ?? ""
    }
}
