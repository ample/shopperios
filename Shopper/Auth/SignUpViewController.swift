//
//  ViewController.swift
//  Shopper
//
//  Created by Benjamin Porter on 12/26/18.
//  Copyright © 2018 Benjamin Porter. All rights reserved.
//

import UIKit
import SwiftyJSON
class SignUpViewController: UIViewController {

    @IBOutlet weak var topContainer: UIView!
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var privacyPolicyButton: UIButton!
    @IBOutlet weak var termsOfServiceButton: UIButton!
    @IBOutlet weak var signupContainer: UIView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var backButton: UIButton!
    
    let api = API()
    let utils = Utils()
    let settings = Settings()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.nameTextField.delegate = self
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard)))
        self.signupContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(signup)))
        
    }
    
    @objc func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.mainContainer.suttleCurve()
        self.signupContainer.suttleCurve()
    }
    
    @IBAction func privacyPolicyTap(_ sender: Any) {
        privacyPolicy()
    }
    
    @IBAction func termsOfServiceTap(_ sender: Any) {
        termsOfServices()
    }
    
    @IBAction func signupTap(_ sender: Any) {
        signup()
    }
    
    
    
    @objc func signup(){
        
        let name = self.nameTextField.text ?? ""
        let email = self.emailTextField.text ?? ""
        let password = self.passwordTextField.text ?? ""
        
        self.api.signup(name:name,email: email, password: password) { (response:Dictionary) in
            print(response)
            let status = response["status"] as! Bool
            if(status){
                var user = User()
                user.build(json: response["data"] as! JSON)
                self.settings.store_user(user: user)
                DispatchQueue.main.async {
                    self.home()
                }
            }else{
                let json = (response["data"] as! JSON).dictionaryValue
                let message = json["message"]?.stringValue ?? ""
                DispatchQueue.main.async {
                    self.utils.showError(title:"Error",message:message)
                }
            }
        }
        
    }
    
    func home(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Home")
        self.view.window?.rootViewController = vc
    }
    
    func goHome(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Home")
        self.view.window?.rootViewController = vc
    }
    
    func privacyPolicy(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Info") as! InfoViewController
        vc.titleStr = "Privacy Policy"
        vc.key = "privacy_policy"
        self.present(vc, animated: true, completion: nil)
    }
    
    func termsOfServices(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Info") as! InfoViewController
        vc.titleStr = "Terms Of Service"
        vc.key = "terms_of_service"
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func backTap(_ sender: Any) {
        
            let sb = UIStoryboard(name: "Main", bundle: nil)
            let vc = sb.instantiateViewController(withIdentifier: "Initial")
            self.view.window?.rootViewController = vc
    }
}


extension SignUpViewController:UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == self.nameTextField){
            textField.resignFirstResponder()
            self.emailTextField.becomeFirstResponder()
        }else if(textField == self.emailTextField){
            textField.resignFirstResponder()
            self.passwordTextField.becomeFirstResponder()
        }else if(textField == self.passwordTextField){
            textField.endEditing(true)
        }
        return true
    }
}
