//
//  InfoViewController.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/20/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMobileAds
class InfoViewController: UIViewController {
    
    @IBOutlet weak var topContainer: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var infoTextView: UITextView!
    @IBOutlet weak var adContainer: UIView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var key:String = ""
    var txt:String = ""
    var titleStr:String = ""
    
    let api = API()
    let utils = Utils()
    let constants = Constants()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.topLabel.text = titleStr
        
        if(key.count > 0){
            self.api.getSetting(key: key) { (response:Dictionary) in
                print(response)
                let status = (response["status"] as! Bool)
                if(status){
                    
                    let data = (response["data"] as! JSON).dictionaryValue
                    let val = data["value"]?.string ?? ""
                    DispatchQueue.main.async {
                        self.infoTextView.text = val
                    }
                    
                }else{
                    let json = (response["data"] as! JSON).dictionaryValue
                    let message = json["message"]?.stringValue ?? ""
                    DispatchQueue.main.async {
                        self.utils.showError(title:"Error",message:message)
                    }
                }
            }
        }else{
            self.infoTextView.text = self.txt
        }
        
        configureBanner()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       // self.adContainer.suttleCurve()
        self.mainContainer.suttleCurve()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        bannerView.load(getAdRequest())
    }
    
    @IBAction func backTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getAdRequest()->GADRequest{
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        return request
    }
    
    func configureBanner(){
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        if(self.constants.liveAd){
            //bannerView.adUnitID = "ca-app-pub-4573097868659019/4523591802"
        }
        bannerView.rootViewController = self
        bannerView.delegate = self
    }
    
}

extension InfoViewController:GADBannerViewDelegate{
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Got ad")
        self.utils.logAdView()
    }
}
