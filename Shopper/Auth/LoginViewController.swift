//
//  LoginViewController.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/17/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit
import SwiftyJSON
class LoginViewController: UIViewController {
    
    let settings = Settings()
    let api = API()
    let utils = Utils()
    
    @IBOutlet weak var topContainer: UIView!
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var loginContainer: UIView!
    @IBOutlet weak var privacyPolicyButton: UIButton!
    @IBOutlet weak var termsOfServiceButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.emailTextField.delegate = self
        self.passwordTextField.delegate = self
        
        self.loginContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(login)))
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(viewTap)))
    
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let user = self.settings.get_user()
        if(user.id >= 0){
            home()
            return
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.mainContainer.suttleCurve()
        self.loginContainer.suttleCurve()
    }
    
    @IBAction func privacyPolicyTap(_ sender: Any) {
        privacyPolicy()
    }
    
    @IBAction func termsOfServiceTap(_ sender: Any) {
        termsOfServices()
    }
    
    @IBAction func loginTap(_ sender: Any) {
        login()
    }
    
    @objc func login(){
        
        let email = self.emailTextField.text ?? ""
        let password = self.passwordTextField.text ?? ""
        
        self.api.login(email: email, password: password) { (response:Dictionary) in
            print(response)
            let status = response["status"] as! Bool
            if(status){
                var user = User()
                user.build(json: response["data"] as! JSON)
                self.settings.store_user(user: user)
                DispatchQueue.main.async {
                    self.home()
                }
            }else{
                let json = (response["data"] as! JSON).dictionaryValue
                let message = json["message"]?.stringValue ?? ""
                DispatchQueue.main.async {
                    self.utils.showError(title:"Error",message:message)
                }
            }
        }
    }
    
    func home(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Home")
        self.view.window?.rootViewController = vc
    }
    
    @objc func viewTap(){
        self.view.endEditing(true)
    }
    
    func privacyPolicy(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Info") as! InfoViewController
        vc.titleStr = "Privacy Policy"
        vc.key = "privacy_policy"
        self.present(vc, animated: true, completion: nil)
    }
    
    func termsOfServices(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Info") as! InfoViewController
        vc.titleStr = "Terms Of Service"
        vc.key = "terms_of_service"
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func backTap(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Initial")
        self.view.window?.rootViewController = vc
    }
    
}


extension LoginViewController:UITextFieldDelegate{
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         if(textField == self.emailTextField){
            textField.resignFirstResponder()
            self.passwordTextField.becomeFirstResponder()
        }else if(textField == self.passwordTextField){
            textField.endEditing(true)
            self.login()
        }
        return true
    }
}
