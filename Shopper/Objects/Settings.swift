//
//  Settings.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/19/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit

class Settings: NSObject {
    
    let settings = UserDefaults.standard
    let user_key = "shopper_user"
    let last_full_screen_ad_key = "shopper_lastFullScreenAd"
    let ad_show_timestamp = "shopper_adShowTimestamp"
    
    public func store_user(user:User){
        
        settings.set(try? PropertyListEncoder().encode(user), forKey: user_key)
        
        settings.synchronize()
        
    }
    
    public func get_user()->User{
        
        if(settings.object(forKey: user_key) != nil){
            if let data = settings.value(forKey:user_key) as? Data {
                if let user = try? PropertyListDecoder().decode(User.self, from: data){
                    return user
                }else{
                    return User()
                }
                
            }
        }
        
        return User()
    }
    
    public func setBool(bool:Bool,key:String){
        settings.set(bool, forKey: key)
        settings.synchronize()
    }
    
    public func getBool(key:String)->Bool{
        return settings.bool(forKey:key)
    }
    
    public func setString(string:String,key:String){
        settings.set(string, forKey: key)
        settings.synchronize()
    }
    
    public func getString(key:String)->String{
        if(!(settings.string(forKey:key) != nil)){
            return ""
        }
        return settings.string(forKey:key)!
    }
    
}
