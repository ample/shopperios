//
//  HomeViewController.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/18/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit
import SwiftyJSON
class HomeViewController: UIViewController {
    
    let settings = Settings()
    let utils = Utils()
    let api = API()
    var user = User()
    var searchProducts:[Product] = []
    var list:[ListCat] = []
    let constants = Constants()
    
    var isKeyboardShowing = false
    
    @IBOutlet weak var topContainer: UIView!
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addContainer: UIView!
    @IBOutlet weak var addTextField: UITextField!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var keyboardArrowButton: UIButton!
    @IBOutlet weak var noItemContainer: UIView!
    @IBOutlet weak var settingsButtonTopConstraint: NSLayoutConstraint!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.addTextField.delegate = self
        
        if(self.utils.getPhoneType() == "iPhone X" || self.utils.getPhoneType() == "iPhone XS" || self.utils.getPhoneType() == "iPhone XS Max" || self.utils.getPhoneType() == "iPhone XR"){
            self.settingsButtonTopConstraint.constant += 10
        }
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.user = self.settings.get_user()
        if(self.user.id < 0){
            showLogin()
            return
        }
        
        self.getList()
        
        showAd()
        
       
    }
    
    func showAd(){
        let lastAdTimestamp = self.settings.getString(key: self.settings.ad_show_timestamp)
        print("----")
        print(lastAdTimestamp)
        if(lastAdTimestamp != "" && !self.utils.canShowAd(lastAdTimestamp: lastAdTimestamp)){
            print("return")
            return
        }
        
        self.settings.setString(string: self.utils.get_utc_timestamp(), key: self.settings.ad_show_timestamp)
        print("Show ad")
        self.present(self.utils.getAdContainer(), animated: true, completion: nil)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.mainContainer.suttleCurve()
        self.addContainer.suttleCurve()
    }

    @IBAction func settingsTap(_ sender: Any) {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Settings") as! SettingsViewController
        vc.homeVC = self 
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func addTap(_ sender: Any) {
        self.searchProducts.removeAll()
        let text = prepareAddText()
        self.api.productSearch(q: text,user_id: self.user.id) { (response:Dictionary) in
            print("search")
            print(response)
            let status = response["status"] as! Bool
            if(status){
                let json = (response["data"] as! JSON).dictionaryValue
                let results = json["results"]?.arrayValue
                for result in results!{
                    var product = Product()
                    product.build(json: result)
                    self.searchProducts.append(product)
                }
                if(self.searchProducts.count == 0){
                    self.api.createProduct(productName: text, completion: { (response:Dictionary) in
                        print("Create")
                        print(response)
                        let data = (response["data"] as! JSON).dictionaryValue
                        let productId = data["productId"]?.intValue
                        if(productId! < 0){
                            DispatchQueue.main.async {
                                self.utils.showError(title:"Error",message:"Error adding item to list")
                            }
                        }else{
                            self.addItem(categoryId: 9999, productId: productId!, userId: self.user.id)
                        }
                    })
                }else if(self.searchProducts.count == 1){
                    let categoryId = self.searchProducts.first?.category.id ?? -1
                    let productId = self.searchProducts.first?.id ?? -1
                    self.addItem(categoryId: categoryId, productId: productId, userId: self.user.id)
                }else{
                    //more than 1
                }
                
            }else{
                let json = (response["data"] as! JSON).dictionaryValue
                let message = json["message"]?.stringValue ?? ""
                DispatchQueue.main.async {
                    self.utils.showError(title:"Error",message:message)
                }
            }
        }
    }
    
    func prepareAddText()->String{
        var text = self.addTextField.text ?? ""
        text = text.capitalized
        text = text.trimmingCharacters(in: .whitespaces)
        return text
    }
    
    func addItem(categoryId:Int, productId:Int, userId:Int){
        self.api.addItem(userId: userId, categoryId: categoryId, productId: productId, completion: { (response:Dictionary) in
            
            let status = response["status"] as! Bool
            if(status){
                
                self.getList()
                DispatchQueue.main.async {
                    self.utils.showSuccess(title: "Item Added", message: "'" + self.prepareAddText() + "' was added to your list!")
                    self.addTextField.text = ""
                }
                
            }else{
                let json = (response["data"] as! JSON).dictionaryValue
                let message = json["message"]?.stringValue ?? ""
                DispatchQueue.main.async {
                    self.utils.showError(title:"Error",message:message)
                }
            }
            
        })
    }
    
    @objc func handleKeyboardNotification(notification: NSNotification) {
        
        if let userInfo = notification.userInfo {
            
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! CGRect)
            
            isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            
            bottomConstraint?.constant = isKeyboardShowing ? keyboardFrame.height : 0
            
            UIView.animate(withDuration: 0, delay: 0, options: UIView.AnimationOptions.curveEaseOut, animations: {
                
                self.view.layoutIfNeeded()
                
            }, completion: { (completed) in
                
                DispatchQueue.main.async {
                    if self.isKeyboardShowing {
                        self.keyboardArrowButton.setImage(UIImage(named: "down_arrow"), for: .normal)
                    }else{
                        self.keyboardArrowButton.setImage(UIImage(named: "up_arrow"), for: .normal)
                    }
                }
            })
        }
    }
    
    func showLogin(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Login")
        self.view.window?.rootViewController = vc
    }
    
    func getList(){
        self.list.removeAll()
        self.api.getList(userId: self.user.id) { (response:Dictionary) in
            let status = response["status"] as! Bool
            if(status){
                let data = (response["data"] as! JSON).dictionaryValue
                let list_json = data["list"]?.dictionaryValue
                let list_keys = list_json?.keys
                self.createListCats(list_keys: list_keys!)
                for key in list_keys!{
                    for item_json in list_json![key]!.arrayValue{
                        var item = ListItem()
                        item.build(json: item_json)
                        
                        var count = 0
                        for temp in self.list{
                            if(temp.name == key){
                                self.list[count].items.append(item)
                            }
                            count += 1
                        }
                    }
                }
                DispatchQueue.main.async {
                    if(self.list.count < 1){
                        self.tableView.isHidden = true
                    }else{
                        self.tableView.isHidden = false
                    }
                    self.tableView.reloadData()
                }
            }else{
                let json = (response["data"] as! JSON).dictionaryValue
                let message = json["message"]?.stringValue ?? ""
                DispatchQueue.main.async {
                    self.utils.showError(title:"Error",message:message)
                }
            }
        }
        
    }
    
    func createListCats(list_keys:Dictionary<String,JSON>.Keys){
        var keys:[String] = []
        
        for key in list_keys{
            keys.append(key)
        }
        
        keys = keys.sorted()
        
        for key in keys{
            self.list.append(ListCat(name:key,items:[]))
        }
    }
    
    @IBAction func keyboardArrowTap(_ sender: Any) {
        if(isKeyboardShowing){
            self.view.endEditing(true)
            self.keyboardArrowButton.setImage(UIImage(named: "up_arrow"), for: .normal)
        }else{
            self.addTextField.becomeFirstResponder()
            self.keyboardArrowButton.setImage(UIImage(named: "down_arrow"), for: .normal)
        }
    }
    
    func showSortingSelector(item:ListItem){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Sorting Selector") as! SortingSelectorViewController
        vc.modalPresentationStyle = .overCurrentContext
        vc.item = item
        vc.homeVC = self
        self.present(vc, animated: true, completion: nil)
    }
    
    
}

extension HomeViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.addTap(self)
        return true
    }
}


extension HomeViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension HomeViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 40))
        view.backgroundColor = self.constants.purple
        
        let label = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.size.width-10, height: 40))
        label.textAlignment = .left
        label.text = self.list[section].name
        label.textColor = UIColor.white
        label.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        view.addSubview(label)
        view.suttleCurve()
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.list.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell") as! ListTableViewCell
        
        let items = self.list[indexPath.section].items
        let item = items[indexPath.row]
        
        cell.nameLabel.text = item.product.name
        cell.item = item
        cell.homeVC = self
        
        if(cell.checked){
            cell.checkButton.setImage(UIImage(named: "checkbox_closed"), for: .normal)
        }else{
            cell.checkButton.setImage(UIImage(named: "checkbox_open"), for: .normal)
        }
        
        cell.setNeedsLayout()
        cell.setNeedsDisplay()
        
        return cell
    }
    
}
