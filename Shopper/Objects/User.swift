//
//  User.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/19/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit
import SwiftyJSON
struct User:Codable {
    var id:Int = -1
    var email:String = ""
    var name:String = ""
    
     mutating func build(json:JSON){
        let dict = json["user"].dictionaryValue
        self.id = dict["id"]?.int ?? -1
        self.email = dict["email"]?.stringValue ?? ""
        self.name = dict["name"]?.stringValue ?? ""
    }
}
