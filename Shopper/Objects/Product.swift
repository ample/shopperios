//
//  List.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/19/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit
import SwiftyJSON
struct Product {
    var id:Int = -1
    var name:String = ""
    var category:Category!
    
    mutating func build(json:JSON){
        let dict = json.dictionaryValue
        self.id = dict["productId"]?.int ?? -1
        self.name = dict["productName"]?.string ?? ""
        
        let categoryId = dict["categoryId"]?.int ?? -1
        let categoryName = dict["categoryName"]?.string ?? ""
        
        self.category = Category(id:categoryId,name:categoryName)
    }
}
