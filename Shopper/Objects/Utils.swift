//
//  Utils.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/17/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit
import SwiftMessages
import GoogleMobileAds
class Utils: NSObject {
    
    let api = API()
    let settings = Settings()

    override init() {
        super.init()
    }
    
    public func showError(title:String,message:String){
        let view = MessageView.viewFromNib(layout: .cardView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🤔"].sm_random()!
        view.configureContent(title: title, body: message, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        view.button?.removeFromSuperview()
        
        // Show the message.
        SwiftMessages.show(view: view)
    }
    
    public func showSuccess(title:String,message:String){
        let view = MessageView.viewFromNib(layout: .cardView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.success)
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        // Set message title, body, and icon. Here, we're overriding the default warning
        // image with an emoji character.
        let iconText = ["🙂"].sm_random()!
        view.configureContent(title: title, body: message, iconText: iconText)
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        view.button?.removeFromSuperview()
        
        // Show the message.
        SwiftMessages.show(view: view)
    }
    
    public func getAdContainer()->AdContainerViewController{
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Ad Container") as! AdContainerViewController
        return vc
    }
    
    public func getAppVersion()->String{
        let nsObject: AnyObject? = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as AnyObject
        return nsObject as! String
    }
    
    public func getOSVersion()->String{
        return UIDevice.current.systemVersion
    }
    public func getPhoneType()->String{
        return UIDevice.current.modelName
    }
    public func getDeviceID()->String{
        return UIDevice.current.identifierForVendor!.uuidString
    }
    
    public func storeDevice(){
        self.api.storeDevice(user_id: Settings().get_user().id) { (response:Dictionary) in
        }
        
    }
    
    public func logAppOpen(){
        let user = self.settings.get_user()
        self.api.log(user_id: user.id, type:"Open App") { (response:Dictionary) in
        }
    }
    
    public func logAdView(){
        let user = self.settings.get_user()
        self.api.log(user_id: user.id, type:"View Ad") { (response:Dictionary) in
        }
    }
    
    public func logAdTap(){
        let user = self.settings.get_user()
        self.api.log(user_id: user.id, type:"Tap Ad") { (response:Dictionary) in
        }
    }
    
    public func get_utc_timestamp()->String{
        let date = Date()
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let ts = dateFormatter.string(from: date)
        return ts
    }
    
    func canShowAd(lastAdTimestamp:String)->Bool{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let date = dateFormatter.date(from: lastAdTimestamp)
        
        let now = Date()
        let now_string = dateFormatter.string(from: now)
        let now_utc = dateFormatter.date(from: now_string)
        
        
        let diff = Date().timeIntervalSince(date ?? Date())
        
        let diff_hours = diff / 3600
        if(diff_hours < 1){
            return false
        }
        
        return true
    }
    
    
}

extension UIView {
    
    func roundedView(){
        layer.cornerRadius = frame.size.width/2
        layer.masksToBounds = true
    }
    
    func suttleCurve(){
        layer.cornerRadius = 10
        layer.masksToBounds = true
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
    
}


public extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone8,4":                               return "iPhone SE"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPhone11,2":                              return "iPhone XS"
        case "iPhone11,4","iPhone11,6":                 return "iPhone XS Max"
        case "iPhone11,8":                              return "iPhone XR"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad6,11", "iPad6,12":                    return "iPad 5"
        case "iPad7,5", "iPad7,6":                      return "iPad 6"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4":                      return "iPad Pro 9.7 Inch"
        case "iPad6,7", "iPad6,8":                      return "iPad Pro 12.9 Inch"
        case "iPad7,1", "iPad7,2":                      return "iPad Pro 12.9 Inch 2. Generation"
        case "iPad7,3", "iPad7,4":                      return "iPad Pro 10.5 Inch"
        case "AppleTV5,3":                              return "Apple TV"
        case "AppleTV6,2":                              return "Apple TV 4K"
        case "AudioAccessory1,1":                       return "HomePod"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}

