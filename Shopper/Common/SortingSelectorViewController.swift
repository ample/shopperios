//
//  SortingSelectorViewController.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/18/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMobileAds
class SortingSelectorViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet weak var saveContainer: UIView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var adContainer: UIView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var item:ListItem!
    var homeVC:HomeViewController!
    var categories:[Category] = []
    let api = API()
    let settings = Settings()
    var user:User!
    let utils = Utils()
    let constants = Constants()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)
        self.picker.dataSource = self
        self.picker.delegate = self
        
        self.saveContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(save)))
        
        configureBanner()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.topLabel.text = self.item.product.name
        self.user = self.settings.get_user()
        
        self.api.getCategories { (response:Dictionary) in
            let status = (response["status"] as! Bool)
            if(status){
                let data = (response["data"] as! JSON).dictionary
                let cats_json = data!["categories"]!.arrayValue
                for cat_json in cats_json{
                    var category = Category()
                    category.build(json:cat_json)
                    self.categories.append(category)
                }
                DispatchQueue.main.async {
                    self.picker.reloadAllComponents()
                }
            }
        }
        
        bannerView.load(getAdRequest())
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.saveContainer.suttleCurve()
        self.containerView.suttleCurve()
        //self.adContainer.suttleCurve()
    }

    @IBAction func saveTap(_ sender: Any) {
        
        let selectedCategoryId = self.categories[picker.selectedRow(inComponent: 0)].id
        
        self.api.addRule(product_id: self.item.product.id, category_id: selectedCategoryId, user_id: self.user.id) { (response:Dictionary) in
            print(response)
            let status = (response["status"] as! Bool)
            if(status){
                DispatchQueue.main.async {
                    self.homeVC.getList()
                    self.dismiss(animated: true, completion: nil)
                }
            }else{
               self.utils.showError(title: "Error", message: "Error updating setting, please try again")
            }
        }
    }
    
    @objc func save(){
        saveTap(self)
    }
    
    @IBAction func closeTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getAdRequest()->GADRequest{
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        return request
    }
    
    func configureBanner(){
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        if(self.constants.liveAd){
            //bannerView.adUnitID = "ca-app-pub-4573097868659019/4523591802"
        }
        bannerView.rootViewController = self
        bannerView.delegate = self
    }
}


extension SortingSelectorViewController:UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.categories.count
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.categories[row].name
    }
    
}

extension SortingSelectorViewController:UIPickerViewDelegate{
    
}

extension SortingSelectorViewController:GADBannerViewDelegate{
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Got ad")
        self.utils.logAdView()
    }
}
