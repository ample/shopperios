//
//  ListItem.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/19/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit
import SwiftyJSON

struct ListItem {
    var id:Int = -1
    var product:Product!
    
    mutating func build(json:JSON){
        let dict = json.dictionaryValue
        self.id = (dict["itemId"])?.int ?? -1
        let productId = (dict["productId"])?.int ?? -1
        let productName = (dict["productName"])?.string ?? ""
        let categoryId = (dict["categoryId"])?.int ?? -1
        let categoryName = (dict["categoryName"])?.string ?? ""
        
        self.product = Product(id:productId,name:productName,category:Category(id:categoryId,name:categoryName))
    }
}
