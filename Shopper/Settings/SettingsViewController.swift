//
//  SettingsViewController.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/18/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit
import SwiftMessages
import GoogleMobileAds
class SettingsViewController: UIViewController {

    @IBOutlet weak var topContainer: UIView!
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var mainContainer: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var nameValueLabel: UILabel!
    @IBOutlet weak var emailValueLabel: UILabel!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var clearListButton: UIButton!
    @IBOutlet weak var customSortingButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var adContainer: UIView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    var sections:[[Setting]] = [[]]
    var user = Settings().get_user()
    let constants = Constants()
    let utils = Utils()
    
    var homeVC:HomeViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let nameSetting = Setting(id:"user_name",leftLabel:"Name:",rightLabel:user.name)
        let emailSetting = Setting(id:"user_email",leftLabel:"Email:",rightLabel:user.email)
        let changePasswordSetting = Setting(id:"change_password",leftLabel:"Change Password",rightLabel:"")
        let clearListSetting = Setting(id:"clear_list",leftLabel:"Clear List",rightLabel:"")
        //let customSortingSetting = Setting(id:"custom_sorting",leftLabel:"Custom Sorting",rightLabel:"")
        
        let privacyPolicySetting = Setting(id:"privacy_policy",leftLabel:"Privacy Policy",rightLabel:"")
        let termsOfServiceSetting = Setting(id:"terms_of_service",leftLabel:"Terms Of Service",rightLabel:"")
        
        let logoutSetting = Setting(id:"logout",leftLabel:"Logout",rightLabel:"")
        
        self.sections[0].append(nameSetting)
        self.sections[0].append(emailSetting)
        self.sections.append([changePasswordSetting,clearListSetting])
        self.sections.append([privacyPolicySetting,termsOfServiceSetting])
        self.sections.append([logoutSetting])
        //self.sections[1].append(customSortingSetting)
        
        self.tableView.reloadData()
        
        
        configureBanner()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        bannerView.load(getAdRequest())
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.mainContainer.suttleCurve()
       // self.adContainer.suttleCurve()
    }
    
    @IBAction func backTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.homeVC.getList()
    }
    
    func showClearListPrompt(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Clear List") as! ClearListViewController
        //vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    func showChangePassword(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Change Password") as! ChangePasswordViewController
        //vc.modalPresentationStyle = .overCurrentContext
        self.present(vc, animated: true, completion: nil)
    }
    
    func privacyPolicy(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Info") as! InfoViewController
        vc.titleStr = "Privacy Policy"
        vc.key = "privacy_policy"
        self.present(vc, animated: true, completion: nil)
    }
    
    func termsOfServices(){
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Info") as! InfoViewController
        vc.titleStr = "Terms Of Service"
        vc.key = "terms_of_service"
        self.present(vc, animated: true, completion: nil)
    }
    
    func logout(){
        print("Logout")
        Settings().store_user(user: User())
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let vc = sb.instantiateViewController(withIdentifier: "Initial")
        self.view.window?.rootViewController = vc
    }
    
    func getAdRequest()->GADRequest{
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        return request
    }
    
    func configureBanner(){
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        if(self.constants.liveAd){
            //bannerView.adUnitID = "ca-app-pub-4573097868659019/4523591802"
        }
        bannerView.rootViewController = self
        bannerView.delegate = self
    }
}



extension SettingsViewController:UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let setting = self.sections[indexPath.section][indexPath.row]
        
        switch setting.id {
        case "user_name":
            break
        case "user_email":
            break
        case "change_password":
            showChangePassword()
            break
        case "clear_list":
            showClearListPrompt()
            break
        case "custom_sorting":
            break
        case "privacy_policy":
            privacyPolicy()
            break
        case "terms_of_service":
            termsOfServices()
            break
        case "logout":
            logout()
            break
        default:
            break
        }
        
    }
}

extension SettingsViewController:UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: 10))
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sections[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell") as! SettingsTableViewCell
        let setting = self.sections[indexPath.section][indexPath.row]
        cell.leftLabel.text = setting.leftLabel
        cell.rightLabel.text = setting.rightLabel
        
        return cell
    }
    
    
    
}

extension SettingsViewController:GADBannerViewDelegate{
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Got ad")
        self.utils.logAdView()
    }
}
