//
//  Setting.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/20/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit

struct Setting {
    var id:String = ""
    var leftLabel:String = ""
    var rightLabel:String = ""
}
