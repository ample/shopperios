//
//  ChangePasswordViewController.swift
//  Shopper
//
//  Created by Benjamin Porter on 1/20/19.
//  Copyright © 2019 Benjamin Porter. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMobileAds
class ChangePasswordViewController: UIViewController {

    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var changePasswordContainer: UIView!
    @IBOutlet weak var changePasswordButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var adContainer: UIView!
    @IBOutlet weak var bannerView: GADBannerView!
    
    let api = API()
    var user:User!
    let settings = Settings()
    let utils = Utils()
    let constants = Constants()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.view.backgroundColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.5)

        // Do any additional setup after loading the view.
        self.changePasswordContainer.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(change)))
        self.user = self.settings.get_user()
        
        configureBanner()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        bannerView.load(getAdRequest())
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.containerView.suttleCurve()
        //self.adContainer.suttleCurve()
        self.changePasswordContainer.suttleCurve()
        
    }
    
    @IBAction func changePasswordTap(_ sender: Any) {
        self.api.changePassword(user_id: self.user.id, oldPassword: self.oldPasswordTextField.text ?? "", newPassword: self.newPasswordTextField.text ?? "") { (response:Dictionary) in
            
            let status = (response["status"] as! Bool)
            if(status){
                DispatchQueue.main.async {
                    self.utils.showSuccess(title:"Success",message:"Your password has been changed")
                    self.closeTap(self)
                }
            }else{
                let json = (response["data"] as! JSON).dictionaryValue
                let message = json["message"]?.stringValue ?? ""
                DispatchQueue.main.async {
                    self.utils.showError(title:"Error",message:message)
                }
            }
            
        }
    }
    
    @objc func change(){
        self.changePasswordTap(self)
    }
    @IBAction func closeTap(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func getAdRequest()->GADRequest{
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        return request
    }
    
    func configureBanner(){
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        if(self.constants.liveAd){
            //bannerView.adUnitID = "ca-app-pub-4573097868659019/4523591802"
        }
        bannerView.rootViewController = self
        bannerView.delegate = self
    }
}

extension ChangePasswordViewController:GADBannerViewDelegate{
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("Got ad")
        self.utils.logAdView()
    }
}
